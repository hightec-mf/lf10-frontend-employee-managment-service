import {Pipe, PipeTransform} from "@angular/core";
import {Employee} from "../dtos/employee";

@Pipe({name:'filterOnName'})
export class FilterOnName implements PipeTransform {
  transform(list : Employee[] | null, startsWith : string) {
    if (!list) return;
    let lowerStartsWith = startsWith.toLowerCase();

    return list.filter(item =>
      item.lastName?.toLocaleLowerCase().startsWith(lowerStartsWith)
      || item.firstName?.toLocaleLowerCase().startsWith(lowerStartsWith)
    );
  }
}
