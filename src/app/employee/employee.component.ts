import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {Employee} from "../dtos/employee";
import {AuthService} from "../services/auth.service";
import {EmployeeService} from "../services/employee.service";

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  employeeData: Employee | undefined;
  editMode: boolean = false;
  errorMessage: string = "";

  constructor(private router: Router, private authService: AuthService, private empService: EmployeeService) {
  }

  ngOnInit(): void {
    if (this.authService.isExpired()) {
      this.router.navigate(['']).then();
    }

    if (this.empService.selectedId == 0) {
      this.goBack();
    }

    this.getData();
  }

  getData() {
    this.empService.getEmployeeById(this.empService.selectedId).subscribe(
      data => this.employeeData = data
    );
  }

  update() {
    if (this.employeeData) {
      this.empService.updateEmployee(this.employeeData).subscribe(data => {
          this.employeeData = data;
          this.editMode = false;
          this.errorMessage = "";
        },
        error => this.errorMessage = error.message
      );
    }
  }

  goBack() {
    history.back();
  }
}
