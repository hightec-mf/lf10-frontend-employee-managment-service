import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Bearer} from "../dtos/bearer";

const TOKEN_KEY = 'token';
const EXPIRATION = 'exp';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isUserLoggedIn: boolean = false;

  constructor(private http: HttpClient) { }

  login(username: string, password: string) : Observable<Bearer> {
    let options = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    };

    let body = new URLSearchParams();
    body.set('grant_type', 'password');
    body.set('client_id', 'employee-management-service');
    body.set('username', username);
    body.set('password', password);

    return this.http.post<Bearer>('/auth', body.toString(), options);
  }

  saveAuthData(token: Bearer) {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.removeItem(EXPIRATION);

    let expiresIn = token.expires_in;

    window.sessionStorage.setItem(TOKEN_KEY, token.access_token);
    window.sessionStorage.setItem(EXPIRATION, AuthService.getExpiration(expiresIn).toString());
  }

  logout() {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.removeItem(EXPIRATION);
  }

  getToken(): string | null {
    return window.sessionStorage.getItem(TOKEN_KEY);
  }

  isExpired(): boolean {
    let expired = window.sessionStorage.getItem(EXPIRATION);

    return expired == null || Date.now() > parseInt(expired);
  }

  private static getExpiration(seconds: number): number {
    return Date.now() + seconds * 1000;
  }
}
