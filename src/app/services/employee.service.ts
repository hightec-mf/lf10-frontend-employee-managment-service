import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Employee} from "../dtos/employee";
import {AuthService} from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  selectedId: number = 0;

  constructor(private http: HttpClient, private auth: AuthService) { }

  getAllEmployees(): Observable<Employee[]> {
    let options = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.auth.getToken()}`)
    }

    return this.http.get<Employee[]>('/backend', options);
  }

  getEmployeeById(id: number) : Observable<Employee> {
    let options = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.auth.getToken()}`)
    }

    return this.http.get(`/backend/${id}`, options);
  }

  createEmployee(e: Employee) : Observable<Employee> {
    let options = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.auth.getToken()}`)
    };

    let body = JSON.stringify(e);

    return this.http.post<Employee>('/backend', body, options);
  }

  deleteEmployee(e: Employee) {
    let options = {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${this.auth.getToken()}`)
    };

    return this.http.delete(`/backend/${e.id}`, options)
  }

  updateEmployee(e: Employee): Observable<Employee> {
    let options = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.auth.getToken()}`)
    }

    let body = JSON.stringify(e);

    return this.http.put<Employee>(`/backend/${e.id}`, body, options)
  }
}
