export interface Bearer {
  access_token : string,
  expires_in : number,
  refresh_expires_in : number,
  refresh_token : string,
  token_type : string,
  not_before_policy : boolean,
  session_state : string,
  scope : string
}
