import {Component, OnInit} from '@angular/core';
import {Employee} from "../dtos/employee";
import {Router} from "@angular/router";
import {AuthService} from "../services/auth.service";
import {EmployeeService} from "../services/employee.service";

@Component({
  selector: 'app-new-employee',
  templateUrl: './new-employee.component.html',
  styleUrls: ['./new-employee.component.css']
})
export class NewEmployeeComponent implements OnInit {
  newEmployee : Employee;
  errorMessage: string = "";

  constructor(private router: Router, private authService: AuthService, private empService: EmployeeService) {
    this.newEmployee = new Employee();
  }

  ngOnInit(): void {
    if (this.authService.isExpired()) {
      this.router.navigate(['']).then();
    }
  }

  create() {
    this.empService.createEmployee(this.newEmployee).subscribe(
      data => {
        this.empService.selectedId = <number>data.id;
        this.router.navigate(['employee']).then();
      },
      err => this.errorMessage = err.message
    );
  }

  goBack() {
    history.back();
  }
}
