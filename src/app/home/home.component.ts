import {Component, OnInit} from '@angular/core';
import {Employee} from "../dtos/employee";
import {Router} from "@angular/router";
import {Observable, of} from "rxjs";
import {AuthService} from "../services/auth.service";
import {EmployeeService} from "../services/employee.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  employees: Observable<Employee[]>;
  search: string = "";

  constructor(private router: Router, private authService: AuthService, private empService: EmployeeService) {
    this.employees = of([]);
  }

  ngOnInit(): void {
    if (this.authService.isExpired()) {
      this.router.navigate(['login']).then();
    }

    this.updateEmployeeData();
  }

  updateEmployeeData() {
    this.employees = this.empService.getAllEmployees();
  }

  editEmployee(e: Employee) {
    this.empService.selectedId = <number>e.id;
    this.router.navigate(['employee']).then();
  }

  deleteEmployee(e: Employee) {
    if (!confirm(`Willst du wirklich ${e.firstName} ${e.lastName} löschen?`))
      return;

    this.empService.deleteEmployee(e).subscribe(
      () => {
        console.log(`Datensatz mit der ID ${e.id} wurde erfolgreich gelöscht.`);
        this.updateEmployeeData();
      },
      _ => console.error(`Datensatz mit der ID ${e.id} konnte nicht gelöscht werden. :(`)
    );
  }

  createNewEmployee() {
    this.router.navigate(['employee/create']).then();
  }
}
