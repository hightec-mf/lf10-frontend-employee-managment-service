import {Component, OnInit} from '@angular/core';
import {Login} from "../dtos/login";
import {Router} from "@angular/router";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  isLoginFailed: boolean = false;
  errorMessage = '';

  loginData: Login = {
    username: "",
    password: ""
  };

  constructor(private router: Router, private auth: AuthService) {
  }

  ngOnInit(): void {
    if (!this.auth.isExpired()) {
      this.goHome();
    }
  }

  login() {
    this.auth.login(this.loginData.username, this.loginData.password).subscribe(
      data => {
        this.auth.saveAuthData(data);
        this.goHome();
      },
      err => {
        this.loginFailed(err.message);
      }
    )
  }

  goHome() {
    this.router.navigate(['home']).then();
  }

  loginFailed(message: string) {
    this.loginData = {
      username: "",
      password: ""
    }
    this.errorMessage = message;
    this.isLoginFailed = true;
  }
}
